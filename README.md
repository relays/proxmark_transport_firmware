## Proxmark client with TfL reader command

Fork of [Iceman Proxmark firmware repo](https://github.com/RfidResearchGroup/proxmark3).

See original [README.md](README_pm.md).

This repository contains the Proxmark firmware, with the necessary modifications to add a new reader command, which acts as a TfL reader (sends a sequence of magic bytes before the WUPA/ATQA 14a command). The actual magic bytes values have been removed.
